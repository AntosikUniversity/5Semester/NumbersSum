package ru.antosik.numberssum.Classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SumReducer {
    public static List<Integer> read(Scanner scanner) {
        List<Integer> list = new ArrayList<>();
        while (scanner.hasNextInt()) {
            list.add(scanner.nextInt());
        }
        return list;
    }

    public static int sum(List<Integer> numbers) {
        if (isNull(numbers)) return 0;

        List<Integer> results = new ArrayList<>(numbers);

        do {
            System.out.println(results);
            results = sumStage(results);
        } while (results.size() != 1);


        System.out.println(results);
        return results.get(0);
    }

    private static List<Integer> sumStage(List<Integer> numbers) {
        List<Integer> list = new ArrayList<>();

        int i = 0, len = numbers.size();

        while (i < len - 1) {
            list.add(numbers.get(i) + numbers.get(i + 1));
            i += 2;
        }

        if (i == len - 1)
            list.add(numbers.get(len - 1));

        return list;
    }

    private static boolean isNull(List<Integer> list) {
        return list.isEmpty() || list.stream().allMatch(integer -> integer == 0);
    }
}
