package ru.antosik.numberssum;

import ru.antosik.numberssum.Classes.SumReducer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sum;

        // Scanner example
        String filename = "resources/sample.txt";
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            List<Integer> items = SumReducer.read(scanner);
            sum = SumReducer.sum(items);
            System.out.printf("Scanner [filename - %s] - %d %n", filename, sum);
        } catch (FileNotFoundException e) {
            System.err.printf("File \"%s\" not found! %n", filename);
        }

        // Collection example
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        sum = SumReducer.sum(List.of(0, 1234567));
        System.out.printf("List - %d %n", sum);
    }
}
