## NumbersSum
### Description (RU)
Выполнить попарное суммирование произвольного конечного ряда чисел
следующим образом: на первом этапе суммируются попарно рядом стоящие
числа, на втором этапе суммируются результаты первого этапа и т.д. до тех
пор, пока не останется одно число. 

**Примечание: использовать коллекции
JAVA, входные данные передаются через текстовый файл.**